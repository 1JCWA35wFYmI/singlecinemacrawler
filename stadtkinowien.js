const CrawlE = require('crawl-e')


let crawlE = new CrawlE(
	{
		cinemas: [{
			
			website: 'http://stadtkinowien.at/stadtkino/kinoprogramm/',
			name: 'KINO EBENSE',
		    address: 'Giselakai 11, A-5020 Salzburg',
		    phone: '0662-87 31 00'
			
		}],
		 showtimes: {
      url: 'http://stadtkinowien.at/stadtkino/kinoprogramm/',
      movies: {
        box: '.wrapper',
        title: 'h1 > a',
        showtimes: {
            box: '.box.schedule',
            date: 'h1',
            dateLocale: 'de',
            dateFormat: 'dddd DD. MMMM',
            time: {
              selector: '.entry time',
              mapper: value => value.trim().split(" ")[0].trim()
            },
            timeFormat: 'HH.mm',
            timeLocale: 'de'
          }
      }
  }
})
crawlE.crawl()