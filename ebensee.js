const CrawlE = require('crawl-e')

let crawlE = new CrawlE(
	{
		cinemas: [{

			website: 'http://www.kino-ebensee.at',
			name: 'KINO EBENSE',
		    address: 'Giselakai 11, A-5020 Salzburg',
		    phone: '0662-87 31 00'

		}],
		 showtimes: {
			url: 'https://www.kino-ebensee.at/veranstaltungsprogramm.html',
			movies: {
				box: '.eventWrap',
				title: '.eventHeader',
				showtimes: {
					parser: (showtimesContainer, context) => {
				        let formattedShowtimes = showtimesContainer.find('.date.single').text()
				        let lines = formattedShowtimes.trim().split("\n")
				        				.map(line => { return line.trim() })
				        				.filter(line => { return line != ''})

				        let showtimes = lines.map(date_str => {
				          let date = date_str.split(' ')[1].trim()
				          let time = date_str.split(' ')[2].trim()
				          return {
				            movie_title: context.movie.title,
				            start_at: date + 'T' + time,
				          }
				        })

				        return showtimes
		      		}
				}
			}
  		}
	})

crawlE.crawl()
